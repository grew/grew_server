open Eliom_lib
open Eliom_content
open Html.D
open Gs_utils
open Gs_main
open Grewlib

let status json = 
  match json |> Yojson.Basic.Util.member "status" |> Yojson.Basic.Util.to_string with
  | "OK" -> "OK"
  | _ -> Yojson.Basic.pretty_to_string json
  
let ms_from start = (Unix.gettimeofday () -. start) *. 1000.

module Grew_server_app =
  Eliom_registration.App (
  struct
    let application_name = "grew_server"
    let global_data_path = None
  end)

(* -------------------------------------------------------------------------------- *)
(* ping *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["ping"])
    ~meth:(Eliom_service.Post (Eliom_parameter.unit, Eliom_parameter.unit))
    (fun () () ->
       Log.info "<ping>";
       Lwt.return ("{}" , "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* newProject *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["newProject"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id")
      ))
    (fun () project_id ->
       let start = Unix.gettimeofday () in
       let json = wrap new_project project_id in
       Log.info "{%gms} <newProject> project_id=[%s] ==> %s" (ms_from start) project_id (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* getProjects *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["getProjects"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit, Eliom_parameter.unit
      ))
    (fun () () ->
       let start = Unix.gettimeofday () in
       let json = wrap get_projects () in
       Log.info "{%gms} <getProjects> ==> %s" (ms_from start) (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* getUserProjects *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["getUserProjects"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "user_id")
      )) (* XXX *)
    (fun () user_id ->
       let start = Unix.gettimeofday () in
       let json = wrap get_user_projects user_id in
       Log.info "{%gms} <getUserProjects#1> user_id=[%s] ==> %s" (ms_from start) user_id (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* eraseProject *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["eraseProject"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id")
      ))
    (fun () project_id ->
       let start = Unix.gettimeofday () in
       let json = wrap erase_project project_id in
       Log.info "{%gms} <eraseProject> project_id=[%s] ==> %s" (ms_from start) project_id (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* renameProject *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["renameProject"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** string "new_project_id")
      ))
    (fun () (project_id, new_project_id) ->
       let start = Unix.gettimeofday () in
       let json = wrap (rename_project project_id) new_project_id in
       Log.info "{%gms} <renameProject> project_id=[%s] new_project_id=[%s] ==> %s" (ms_from start) project_id new_project_id (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* getProjectConfig *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["getProjectConfig"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id")
      ))
    (fun () project_id ->
       let start = Unix.gettimeofday () in
       let json = wrap get_project_config project_id in
       Log.info "{%gms} <getProjectConfig> project_id=[%s] ==> %s" (ms_from start) project_id (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* updateProjectConfig *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["updateProjectConfig"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** string "config")
      ))
    (fun () (project_id,config) ->
       let start = Unix.gettimeofday () in
       let json = wrap (update_project_config project_id) config in
       Log.info "{%gms} <updateProjectConfig> project_id=[%s] config=[%s] ==> %s" (ms_from start) project_id config (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* newSamples *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["newSamples"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** string "sample_ids")
      ))
    (fun () (project_id,sample_ids) ->
       let start = Unix.gettimeofday () in
       let json = wrap (new_samples project_id) sample_ids in
       Log.info "{%gms} <newSamples> project_id=[%s] sample_ids=[%s] ==> %s" (ms_from start) project_id sample_ids (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )


(* -------------------------------------------------------------------------------- *)
(* getSamples *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["getSamples"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.string "project_id"
      ))
    (fun () (project_id) ->
       let start = Unix.gettimeofday () in
       let json = wrap get_samples project_id in
       Log.info "{%gms} <getSamples> project_id=[%s] ==> %s" (ms_from start) project_id (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* eraseSamples *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["eraseSamples"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** string "sample_ids")
      ))
    (fun () (project_id,sample_ids) ->
       let start = Unix.gettimeofday () in
       let json = wrap (erase_samples project_id) sample_ids in
       Log.info "{%gms} <eraseSamples> project_id=[%s] sample_ids=[%s] ==> %s" (ms_from start) project_id sample_ids (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* renameSample *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["renameSample"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** (string "sample_id" ** string "new_sample_id"))
      ))
    (fun () (project_id,(sample_id,new_sample_id)) ->
       let start = Unix.gettimeofday () in
       let json = wrap (rename_sample project_id sample_id) new_sample_id in
       Log.info "{%gms} <renameSample> project_id=[%s] sample_id=[%s] new_sample_id=[%s] ==> %s" (ms_from start) project_id sample_id new_sample_id (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )


(* -------------------------------------------------------------------------------- *)
(* eraseSentence *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["eraseSentence"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** (string "sample_id" ** string "sent_id"))
      ))
    (fun () (project_id,(sample_id,sent_id)) ->
       let start = Unix.gettimeofday () in
       let json = wrap (erase_sentence project_id sample_id) sent_id in
       Log.info "{%gms} <eraseSentence> project_id=[%s] sample_id=[%s] sent_id=[%s] ==> %s" (ms_from start) project_id sample_id sent_id (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )


(* -------------------------------------------------------------------------------- *)
(* eraseGraphs *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["eraseGraphs"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** (string "sample_id" ** (string "sent_ids" ** string "user_id")))
      ))
    (fun () (project_id,(sample_id,(sent_ids,user_id))) ->
       let start = Unix.gettimeofday () in
       let json = wrap (erase_graphs project_id sample_id sent_ids) user_id in
       Log.info "{%gms} <eraseGraphs> project_id=[%s] sample_id=[%s] sent_ids=[%s] user_id=[%s] ==> %s" (ms_from start) project_id sample_id sent_ids user_id (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )


(* -------------------------------------------------------------------------------- *)
(* getConll *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["getConll"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** (string "sample_id" ** (string "sent_id" ** string "user_id")))
      ))
    (fun () (project_id,(sample_id,(sent_id,user_id))) ->
       let start = Unix.gettimeofday () in
       let json = wrap (get_conll__user project_id sample_id sent_id) user_id in
       Log.info "{%gms} <getConll#1> project_id=[%s] sample_id=[%s] sent_id=[%s] user_id=[%s] ==> %s" (ms_from start) project_id sample_id sent_id user_id (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["getConll"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** (string "sample_id" ** string "sent_id"))
      ))
    (fun () (project_id,(sample_id,sent_id)) ->
       let start = Unix.gettimeofday () in
       let json = wrap (get_conll__sent_id project_id sample_id) sent_id in
       Log.info "{%gms} <getConll#2> project_id=[%s] sample_id=[%s] sent_id=[%s] ==> %s" (ms_from start) project_id sample_id sent_id (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["getConll"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** string "sample_id")
      ))
    (fun () (project_id,sample_id) ->
       let start = Unix.gettimeofday () in
       let json = wrap (get_conll__sample project_id) sample_id in
       Log.info "{%gms} <getConll#3> project_id=[%s] sample_id=[%s] ==> %s" (ms_from start) project_id sample_id (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )


(* -------------------------------------------------------------------------------- *)
(* getUsers *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["getUsers"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id")
      ))
    (fun () project_id ->
       let start = Unix.gettimeofday () in
       let json = wrap get_users__project project_id in
       Log.info "{%gms} <getUsers#1> project_id=[%s] ==> %s" (ms_from start) project_id (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["getUsers"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** string "sample_id")
      ))
    (fun () (project_id,sample_id) ->
       let start = Unix.gettimeofday () in
       let json = wrap (get_users__sample project_id) sample_id in
       Log.info "{%gms} <getUsers#2> project_id=[%s] sample_id=[%s] ==> %s" (ms_from start) project_id sample_id (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["getUsers"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** (string "sample_id" ** string "sent_id"))
      ))
    (fun () (project_id,(sample_id,sent_id)) ->
       let start = Unix.gettimeofday () in
       let json = wrap (get_users__sentence project_id sample_id) sent_id in
       Log.info "{%gms} <getUsers#2> project_id=[%s] sample_id=[%s] sent_id=[%s] ==> %s" (ms_from start) project_id sample_id sent_id (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )



(* -------------------------------------------------------------------------------- *)
(* getSentIds *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["getSentIds"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** string "sample_id")
      ))
    (fun () (project_id,sample_id) ->
       let start = Unix.gettimeofday () in
       let json = wrap (get_sent_ids__sample project_id) sample_id in
       Log.info "{%gms} <getSentIds#1> project_id=[%s] sample_id=[%s] ==> %s" (ms_from start) project_id sample_id (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["getSentIds"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id")
      ))
    (fun () project_id ->
       let start = Unix.gettimeofday () in
       let json = wrap get_sent_ids__project project_id in
       Log.info "{%gms} <getSentIds#2> project_id=[%s] ==> %s" (ms_from start) project_id (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )






(* -------------------------------------------------------------------------------- *)
(* saveConll *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["saveConll"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(file "conll_file" ** (string "project_id" ** string "sample_id"))
      ))
    (fun () (conll_file, (project_id, sample_id)) ->
       let start = Unix.gettimeofday () in
       let json = wrap (save_conll project_id sample_id) conll_file in
       Log.info "{%gms} <saveConll#4> conll_file=[%s] project_id=[%s] sample_id=[%s] ==> %s" (ms_from start) (Eliom_request_info.get_tmp_filename conll_file) project_id sample_id (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* insertConll *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["insertConll"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(file "conll_file" ** (string "project_id" ** (string "sample_id" ** string "pivot_sent_id")))
      ))
    (fun () (conll_file, (project_id, (sample_id, pivot_sent_id))) ->
       let start = Unix.gettimeofday () in
       let json = wrap (insert_conll project_id sample_id conll_file) pivot_sent_id in
       Log.info "{%gms} <insertConll#4> conll_file=[%s] project_id=[%s] sample_id=[%s] pivot_sent_id=[%s]  ==> %s" (ms_from start) (Eliom_request_info.get_tmp_filename conll_file) project_id sample_id pivot_sent_id (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* saveGraph *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["saveGraph"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** (string "sample_id" ** (string "user_id" ** string "conll_graph" )))
      ))
    (fun () (project_id,(sample_id,(user_id,conll_graph))) ->
       let start = Unix.gettimeofday () in
       let json = wrap (save_graph project_id sample_id user_id) conll_graph in
       Log.info "{%gms} <saveGraph> project_id=[%s] sample_id=[%s] user_id=[%s] conll_graph=[%s] ==> %s" (ms_from start) project_id sample_id  user_id conll_graph (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* searchRequestInGraphs *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["searchRequestInGraphs"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** (string "user_ids" ** string "request"))
      ))
    (fun () (project_id,(user_ids,request)) ->
       let start = Unix.gettimeofday () in
       let json = wrap (search_request_in_graphs project_id "[]" user_ids request) [] in
       Log.info "{%gms} <searchRequestInGraphs#1> project_id=[%s] user_ids=[%s] request=[%s] ==> %s" (ms_from start) project_id user_ids request (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["searchRequestInGraphs"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** (string "user_ids" ** (string "request" ** string "clusters")))
      ))
    (fun () (project_id,(user_ids,(request,clusters))) ->
       let cluster_keys = Str.split (Str.regexp " *; *") clusters in
       let start = Unix.gettimeofday () in
       let json = wrap (search_request_in_graphs project_id "[]" user_ids request) cluster_keys in
       Log.info "{%gms} <searchRequestInGraphs#2> project_id=[%s] user_ids=[%s] request=[%s] clusters=[%s] ==> %s" (ms_from start) project_id user_ids request clusters (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

    let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["searchRequestInGraphs"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** (string "sample_ids" ** (string "user_ids" ** string "request")))
      ))
    (fun () (project_id,(sample_ids, (user_ids,request))) ->
       let start = Unix.gettimeofday () in
       let json = wrap (search_request_in_graphs project_id sample_ids user_ids request) [] in
       Log.info "{%gms} <searchRequestInGraphs#3> project_id=[%s] sample_ids=[%s] user_ids=[%s] request=[%s] ==> %s" (ms_from start) project_id sample_ids user_ids request (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["searchRequestInGraphs"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** (string "sample_ids" ** (string "user_ids" ** (string "request" ** string "clusters"))))
      ))
    (fun () (project_id,(sample_ids, (user_ids,(request,clusters)))) ->
       let cluster_keys = Str.split (Str.regexp " *; *") clusters in
       let start = Unix.gettimeofday () in
       let json = wrap (search_request_in_graphs project_id sample_ids user_ids request) cluster_keys in
       Log.info "{%gms} <searchRequestInGraphs#4> project_id=[%s] sample_ids=[%s] user_ids=[%s] request=[%s] clusters=[%s] ==> %s" (ms_from start) project_id sample_ids user_ids request clusters (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )



(* -------------------------------------------------------------------------------- *)
(* tryPackage *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["tryPackage"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** (string "sample_ids" ** (string "user_ids" ** string "package")))
      ))
    (fun () (project_id,(sample_ids,(user_ids,package))) ->
       let start = Unix.gettimeofday () in
       let json = wrap (try_package project_id sample_ids user_ids) package in
       Log.info "{%gms} <tryPackage> project_id=[%s] sample_ids=[%s] user_ids=[%s] package=[%s] ==> %s" (ms_from start) project_id sample_ids user_ids package (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* exportProject *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["exportProject"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** string "sample_ids")
      ))
    (fun () (project_id,sample_ids) ->
       let start = Unix.gettimeofday () in
       let json = wrap (export_project project_id) sample_ids in
       Log.info "{%gms} <exportProject> project_id=[%s] sample_ids=[%s] ==> %s" (ms_from start) project_id sample_ids (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* getLexicon *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["getLexicon"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** (string "user_ids" ** (string "sample_ids" ** string "features")))
      ))
    (fun () (project_id, (user_ids, (sample_ids, features))) ->
       let start = Unix.gettimeofday () in
       let json = wrap (get_lexicon features project_id user_ids) sample_ids in
       Log.info "{%gms} <getLexicon#1> project_id=[%s] user_ids=[%s] sample_ids=[%s] features=[%s] ==> %s" (ms_from start) project_id user_ids sample_ids features (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["getLexicon"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** (string "user_ids" ** (string "sample_ids" ** (string "features" ** int "prune"))))
      ))
    (fun () (project_id, (user_ids, (sample_ids, (features, prune)))) ->
       let start = Unix.gettimeofday () in
       let json = wrap (get_lexicon ~prune features project_id user_ids) sample_ids in
       Log.info "{%gms} <getLexicon#2> project_id=[%s] user_ids=[%s] sample_ids=[%s] features=[%s] ==> %s" (ms_from start) project_id user_ids sample_ids features (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* getPOS *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["getPOS"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** string "sample_ids")
      ))
    (fun () (project_id, sample_ids) ->
       let start = Unix.gettimeofday () in
       let json = wrap (get_pos sample_ids) project_id in
       Log.info "{%gms} <getPOS> project_id=[%s] sample_ids=[%s] ==> %s" (ms_from start) project_id sample_ids (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* getRelations *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["getRelations"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** string "sample_ids")
      ))
    (fun () (project_id, sample_ids) ->
       let start = Unix.gettimeofday () in
       let json = wrap (get_relations sample_ids) project_id in
       Log.info "{%gms} <getRelations> project_id=[%s] sample_ids=[%s] ==> %s" (ms_from start) project_id sample_ids (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* getFeatures *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["getFeatures"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** string "sample_ids")
      ))
    (fun () (project_id, sample_ids) ->
       let start = Unix.gettimeofday () in
       let json = wrap (get_features sample_ids) project_id in
       Log.info "{%gms} <getFeatures> project_id=[%s] sample_ids=[%s] ==> %s" (ms_from start) project_id sample_ids (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )

(* -------------------------------------------------------------------------------- *)
(* relationTables *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["relationTables"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.(string "project_id" ** (string "sample_ids" ** string "user_ids"))
      ))
    (fun () (project_id,(sample_ids,user_ids)) ->
       let start = Unix.gettimeofday () in
       let json = wrap (relation_tables project_id sample_ids) user_ids in
       Log.info "{%gms} <relationTables#> project_id=[%s] sample_ids=[%s] user_ids=[%s] ==> %s" (ms_from start) project_id sample_ids user_ids (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )



(*========================================*)
(* internal use only *)
(*========================================*)

(* -------------------------------------------------------------------------------- *)
(* forceSave *)
(* -------------------------------------------------------------------------------- *)
let _ = Eliom_registration.String.create
    ~path:(Eliom_service.Path ["forceSave"])
    ~meth:(Eliom_service.Post (
        Eliom_parameter.unit,
        Eliom_parameter.string "project_id"
      ))
    (fun () (project_id) ->
       let start = Unix.gettimeofday () in
       let json = wrap force_save project_id in
       Log.info "{%gms} <forceSave> project_id=[%s] ==> %s" (ms_from start) project_id (status json);
       Lwt.return (Yojson.Basic.pretty_to_string json, "text/plain")
    )
