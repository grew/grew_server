open Printf
open Conll
open Grewlib

module String_set = struct
  include String_set
  let to_json t = `List (List.map (fun x -> `String x) (elements t))
end

module String_map = struct
  include String_map
  let to_json value_to_json t =
    let assoc_list = fold
      (fun key value acc ->
        (key, value_to_json value) :: acc
      ) t [] in 
     `Assoc assoc_list
end

(* ================================================================================ *)
exception Error of string

let (warnings: Yojson.Basic.t list ref) = ref []
let warn s = warnings := (`String s) :: !warnings

let wrap fct last_arg =
  warnings := [];
  let json =
    try
      let data = fct last_arg in
      match !warnings with
      | [] -> `Assoc [ ("status", `String "OK"); ("data", data) ]
      | l -> `Assoc [ ("status", `String "WARNING"); ("messages", `List l); ("data", data) ]
    with
    | Error msg -> `Assoc [ ("status", `String "ERROR"); ("message", `String msg) ]
    | Conll_error t -> `Assoc [ ("status", `String "ERROR"); ("message", t) ]
    | Grewlib.Error t -> `Assoc [ ("status", `String "ERROR"); ("message", `String   t) ]
    | exc -> `Assoc [ ("status", `String "ERROR"); ("unexpected exception", `String (Printexc.to_string exc)) ] in
  json

(* ================================================================================ *)
(* Utils *)
(* ================================================================================ *)
let folder_iter fct folder =
  let dh = Unix.opendir folder in
  try
    while true do
      match Unix.readdir dh with
      | "." | ".." -> ()
      | x -> fct x
    done;
    assert false
  with
  | End_of_file -> Unix.closedir dh

let folder_fold fct init folder =
  let dh = Unix.opendir folder in
  let acc = ref init in
  try
    while true do
      match Unix.readdir dh with
      | "." | ".." -> ()
      | file -> acc := fct file !acc
    done;
    assert false
  with
  | End_of_file -> Unix.closedir dh; !acc



let parse_json_string_list param_name string =
  let open Yojson.Basic.Util in
  try string |> Yojson.Basic.from_string |> to_list |> List.map to_string
  with
  | Type_error _ -> raise (Error (sprintf "Ill-formed parameter `%s`. It must be a list of strings" param_name))
  | Yojson.Json_error _ -> raise (Error (sprintf "`%s` parameter is not a valid JSON data" param_name))

let json_assoc_add (key : string) (value: Yojson.Basic.t) (json: Yojson.Basic.t) =
  match json with
  | `Assoc list -> `Assoc ((key, value) :: list)
  | _ -> raise (Error (sprintf "[json_assoc_add] data is not an assoc list"))

(* ================================================================================ *)
(* storage folder *)
(* ================================================================================ *)
let (global : string String_map.t ref) = ref String_map.empty
let set_global key value = global := String_map.add key value !global
let get_global key =
  try String_map.find key !global
  with Not_found -> raise (Error (sprintf "Config error: global parameter `%s` is not set" key))



module Log = struct
  let out_ch = ref stdout

  let time_stamp () =
    let gm = Unix.localtime (Unix.time ()) in
    Printf.sprintf "%02d_%02d_%02d_%02d_%02d_%02d"
      (gm.Unix.tm_year - 100)
      (gm.Unix.tm_mon + 1)
      gm.Unix.tm_mday
      gm.Unix.tm_hour
      gm.Unix.tm_min
      gm.Unix.tm_sec

  let init () =
    let basename = Printf.sprintf "grew_server_%s.log" (time_stamp ()) in
    let filename = Filename.concat (get_global "log") basename in
    out_ch := open_out filename

  let _info s = Printf.fprintf !out_ch "[%s] %s\n%!" (time_stamp ()) s
  let info s = Printf.ksprintf _info s
end
