#!/usr/bin/env python3
import sys
sys.path.append('..')
from gs_utils import *
print('========== [ping]')
ping()

project_id = "__gst__config"

# update config
config = { "test": "config", "value": 12 }

print('========== [getProjectConfig]')
print('       ... project_id -> ' + project_id)
reply = send_request('getProjectConfig', data={'project_id': project_id})
check_reply(reply, config)
