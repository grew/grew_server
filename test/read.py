#!/usr/bin/env python3

from gs_utils import *
print ('========== [getProjects]')
reply = send_request ('getProjects')
print (reply)

project_id = "FraCas-French"
sample_id = "fracas_french_udpipe.retok"

print ('========== [getSamples]')
print ('       ... project_id -> %s' % project_id)
reply = send_request ('getSamples', data={'project_id': project_id})
print (reply)

print ('========== [getConll]')
print ('       ... project_id -> %s' % project_id)
print ('       ... sample_id -> %s' % sample_id)
reply = send_request ('getConll', data={'project_id': project_id, 'sample_id': sample_id})
check_reply_dict(reply, 878)

print ('========== [searchRequestInGraphs]')
request = 'pattern { N[ lemma = "commissaire"] }'
user_ids = "[]"
print ('       ... project_id -> %s' % project_id)
print ('       ... request -> %s' % request)
print ('       ... user_ids -> %s' % user_ids)
reply = send_request ('searchRequestInGraphs', data={'project_id': project_id, 'user_ids': user_ids, 'request': request})
check_reply_list(reply, 43)
