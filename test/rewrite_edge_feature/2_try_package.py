#!/usr/bin/env python3
import sys
sys.path.append('..')
from gs_utils import *
ping()

project_id = "__gst__rewrite_edge_feature"
sample_ids = '["one"]'
user_ids = '{ "one": ["john_doe"] }'


with open('r1.grs', 'r') as f:
  rule1 = f.read()

package = "\n".join ([rule1])

print('========== [tryPackage]')
print('       ... project_id -> %s' % project_id)
print('       ... sample_ids -> %s' % sample_ids)
print('       ... user_ids -> %s' % user_ids)
print('       ... package -> %s' % package)
reply = send_request(
    'tryPackage',
    data={'project_id': project_id, 'package': package, 'sample_ids': sample_ids, 'user_ids': user_ids}
)

with open('expected.conllu', 'r') as f:
  exp = f.read()


check_reply_list (reply, 1)

# TODO: check_value (reply["data"][0]["conll"], exp)

